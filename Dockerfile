FROM centos:7

RUN yum -y install epel-release && yum clean all
RUN yum -y install python-pip
RUN yum -y install gcc
RUN yum -y install http://linuxsoft.cern.ch/wlcg/centos7/x86_64/wlcg-repo-1.0.0-1.el7.noarch.rpm
RUN yum -y install HEP_OSlibs
RUN yum -y install python-devel

RUN pip install lbinstall
RUN lbinstall --root=/opt/lhcb install LBSCRIPTSDEV
RUN lbinstall --root=/opt/lhcb install CMT
RUN mkdir -p /opt/nightlies
RUN source /opt/lhcb/LbLoginDev.sh && cd  /opt/nightlies && lbn-install --debug lhcb-upgrade-hackathon 34
RUN lbinstall --root=/opt/lhcb install LCG_92LHCb_x86_64_centos7_gcc62_opt

# Now dealing with the conditions
RUN yum -y install git
RUN mkdir -p /opt/git-conddb
RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/DDDB.git
RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/LHCBCOND.git
RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/SIMCOND.git
# RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/VELOCOND.git
# RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/DQFLAGS.git
# RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/ONLINE.git
# RUN cd /opt/git-conddb && git clone --bare https://gitlab.cern.ch/lhcb-conddb/STCOND.git

# Now install missing data packages
RUN lbinstall --root=/opt/lhcb install PARAM_common-1.0.0-1
RUN lbinstall --root=/opt/lhcb install PARAM_ChargedProtoANNPIDParam_v1r7-1.0.0-1
RUN lbinstall --root=/opt/lhcb install PARAM_ParamFiles_v8r22-1.0.0-1
RUN lbinstall --root=/opt/lhcb install PARAM_QMTestFiles_v1r3-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_common-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_TCK_HltTCK_v3r19-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_Gen_DecFiles_v30r8-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_TCK_L0TCK_v5r27-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_RawEventFormat_v1r9-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_PRConfig_v1r31-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_FieldMap_v5r7-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_Det_SQLDDDB_v7r10-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_AppConfig_v3r353-1.0.0-1
RUN lbinstall --root=/opt/lhcb install DBASE_LbEnvFix_v0r0
RUN ln -s /opt/lhcb/lhcb/DBASE/LbEnvFix/v0r0 /opt/lhcb/lhcb/DBASE/LbEnvFix/prod

# Create the setenv scripts
RUN source /opt/lhcb/LbLoginDev.sh && CMTPROJECTPATH=/opt/nightlies/lhcb-upgrade-hackathon/34:${CMTPROJECTPATH} lb-run -i --ext=gcc --sh Brunel/TDR > /setenv.sh
RUN echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/lhcb/lcg/releases/LCG_92/gcc/6.2.0/x86_64-centos7/lib' >> /setenv.sh
RUN echo 'export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/opt/lhcb/lcg/releases/LCG_92/gcc/6.2.0/x86_64-centos7/lib64' >> /setenv.sh

